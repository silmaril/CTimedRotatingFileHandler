#!/usr/bin/env bash
echo
cd "${0%/*}"  || exit # cd into directory where script is located
echo "${0%/*}"
venvdir=venv
syspython=python3.9

echo Python interpreter: "$(which $syspython)"
echo Python version: "$($syspython --version)"
echo 
echo Creating/updating virtual environment in $venvdir
$syspython -m venv $venvdir
venvpython=$venvdir/bin/$syspython
echo Venv Python: $venvpython
echo 
echo Updating pip...
$venvpython -m pip install --upgrade pip
$venvpython -m pip install --upgrade wheel setuptools
echo 
echo Updating requirements...
$venvpython -m pip install -r requirements.txt
echo 
echo done.
