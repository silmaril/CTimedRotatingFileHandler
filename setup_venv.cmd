@echo off
echo %date% %time% Setup venv...
SET venvdir=venv
SET PythonVersion=Python39
SET PythonUser=%LocalAppData%\Programs\Python\%PythonVersion%\python.exe
SET PythonGlobal=%ProgramFiles%\%PythonVersion%\python.exe

IF EXIST "%PythonUser%" (
  SET PythonExe="%PythonUser%"
) ELSE (
  IF EXIST "%PythonGlobal%" (
    SET PythonExe="%PythonGlobal%"
  ) ELSE (
    echo ERROR: %PythonVersion% not found!
    echo %date% %time% ERROR: %PythonVersion% not found!
    EXIT /B 1
  )
)

echo Using Python interpreter: %PythonExe%
%PythonExe% --version
echo.

echo Installig library virtualenv...
%PythonExe% -m pip install --upgrade virtualenv

echo Creating/updating virtual environment...
%PythonExe% -m venv %venvdir%
SET venvpython=%venvdir%\Scripts\python.exe

echo Updating pip...
%venvpython% -m pip install --upgrade pip

echo Updating requirements...
%venvpython% -m pip install --upgrade -r requirements.txt

echo %date% %time% Setup venv done.
