Function LogMsg([string]$Msg) {
    $now = Get-Date -Format "yyyy-dd-MM HH:mm:ss"
    Write-Output "" "$now $Msg"
}

$PythonVersion = "Python39"
$venvdir = "venv"
$PythonUser="$env:LOCALAPPDATA\Programs\Python\$PythonVersion\python.exe"
$PythonGlobal="$env:ProgramFiles\$PythonVersion\python.exe"

LogMsg "### Setup virtual Python environment..."

if(Test-Path -Path $PythonUser -PathType Leaf) {
    $PythonExe = $PythonUser
} elseif (Test-Path -Path $PythonGlobal -PathType Leaf) {
    $PythonExe = $PythonGlobal
} else {
    LogMsg "ERROR: $PythonVersion not found!"
    Return 1
}
LogMsg "Using Python interpreter: $PythonExe"
& $PythonExe --version

LogMsg "Installig library virtualenv..."
& $PythonExe -m pip install --upgrade virtualenv

LogMsg "Creating/updating virtual environment..."
& $PythonExe -m venv $venvdir
$VenvPythonExe = "$venvdir\Scripts\python.exe"

LogMsg "Venv: Updating pip ..."
& $VenvPythonExe -m pip install --upgrade pip

LogMsg "Venv: Updating requirements..."
& $VenvPythonExe -m pip install --upgrade -r requirements.txt

LogMsg "### Done."
