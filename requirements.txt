# Requirements for development
-r src/requirements.txt

# Requirements for build process
build>=0.8.0
twine>=4.0.1
